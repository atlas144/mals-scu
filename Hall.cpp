#include "Hall.h"

Hall::Hall(byte pin) : PIN(pin) {}

void Hall::init() const {
  pinMode(PIN, INPUT);
}