// SPDX-License-Identifier: MIT

#define SW_DEBUG 0

#if SW_DEBUG
  #include <SoftwareSerial.h>
  #define sw_serial_def(rx, tx)	SoftwareSerial swSerial(rx, tx)
  
  #define sw_serial_begin(x)		swSerial.begin(x)
  #define sw_serial_print(x)		swSerial.print(x)
  #define sw_serial_println(x)	swSerial.println(x)
#else
  #define sw_serial_def(rx, tx)
  #define sw_serial_begin(x)
  #define sw_serial_print(x)
  #define sw_serial_println(x)
#endif

#include <Protorduino.h>
#include <Srsc.h>
#include <ReturnCode.h>
#include <Severity.h>
#include <Wire.h>
#include "Constants.h"
#include "TwiDevice.h"
#include "Motor.h"
#include "Hall.h"

volatile bool frontInterrupted = false;
volatile bool backInterrupted = false;

Protorduino protorduino(1);
Srsc srsc(Serial);
sw_serial_def(SS_RX_PIN, SS_TX_PIN);

void frontInterrupt() {
  frontInterrupted = true;

  for (Motor motor : MOTORS) {
    motor.stop();
  }
}

void backInterrupt() {
  backInterrupted = true;
  
  for (Motor motor : MOTORS) {
    motor.stop();
  }
}

ISR (PCINT1_vect) {
  for (Hall hall : halls) {
    if (digitalRead(hall.PIN) == HIGH) {
      ++(hall.counter);
    }
  }
}

void twiRequestTask(byte devicePacketType) {
  TwiDevice* device = getDevice(devicePacketType);
  
  if (device != NULL) {
    Wire.requestFrom(device->twiAddress, device->dataSize);

    for (byte i = 0; i < device->dataSize; i++) {
      if (!Wire.available()) {
        break;
      }
      
      device->data[i] = Wire.read();
    }

    ReturnCode result = srsc.writePacket(devicePacketType, device->data, device->dataSize);
    
    if (result == ReturnCode::OK) {
      sw_serial_print(F("[SSC] - OK - packet successfully written - type: "));
      sw_serial_println(devicePacketType);
    } else {
      sw_serial_print(F("[SSC] - ERR - packet writing failed - type: "));
      sw_serial_println(devicePacketType);
    }
  } else {
    sw_serial_print(F("[TWI] - ERR - device not registered - type: "));
    sw_serial_println(devicePacketType);
  }
}

void processBumperContact(bool front) {
  while (srsc.writePacket(front ? F_BUMPER_PT : B_BUMPER_PT) != ReturnCode::OK) {
    delay(PROCESS_BUMPER_COLLISION_DELAY);
  }
}

void bumperTask() {
  if (frontInterrupted) {
    processBumperContact(true);
    frontInterrupted = false;
  }

  if (backInterrupted) {
    processBumperContact(false);
    backInterrupted = false;
  }
}

void hallTask() {
  byte* hallMeasurements = new byte[HALL_PS];

  for (byte i = 0; i < HALL_PS; ++i) {
    hallMeasurements[i] = halls[i].counter;
    halls[i].counter = 0;
  }

  while (srsc.writePacket(HALL_PT, hallMeasurements, HALL_PS) != ReturnCode::OK) {
    delay(PROCESS_HALL_MEASUREMENT_DELAY);
  }

  delete [] hallMeasurements;
}

void onPacketArrivedCallback(PacketType* packetType, byte* payload, byte payloadSize) {
  byte identifier = packetType->identifier;
  
  sw_serial_print(F("[SSC] - OK - packet: "));
  sw_serial_println(identifier);
      
  switch (identifier) {
    case REGISTER_SENSOR_PT: {
      unsigned int tasksDuration = payload[REG_DURATION_LSB_POSITION] + BYTE_MAX * payload[REG_DURATION_MSB_POSITION];

      addDevice(payload[REG_SRSC_PACKET_TYPE_POSITION], payload[REG_TWI_ADDRESS_POSITION], payload[REG_DATA_SIZE_POSITION]);
      srsc.registerPacketType(payload[REG_SRSC_PACKET_TYPE_POSITION], payload[REG_DATA_SIZE_POSITION], Severity::NORMAL);
      protorduino.registerTask(twiRequestTask, payload[REG_SRSC_PACKET_TYPE_POSITION], tasksDuration);

      sw_serial_print(F("[SSC]-[PDN] - OK - packet type and task registered: "));
      sw_serial_println(payload[REG_SRSC_PACKET_TYPE_POSITION]);
      break;
    }
    default:
      if (identifier >= FL_MOTOR_SPEED_PT && identifier <= BR_MOTOR_SPEED_PT) {
        MOTORS[identifier - FL_MOTOR_SPEED_PT].setSpeed(payload[MOTOR_SPEED_POSITION]);
      } else if (identifier >= FL_MOTOR_DIRECTION_PT && identifier <= BR_MOTOR_DIRECTION_PT) {
        Motor motor = MOTORS[identifier - FL_MOTOR_DIRECTION_PT];

        if (payload[MOTOR_DIRECTION_POSITION] == MOTOR_FORWARD) {
          motor.forward();
        } else {
          motor.backward();
        }
      }

      break;
  }
}

void setup() {
  sw_serial_begin(SW_SERIAL_BR);
  sw_serial_println(F("[SWS] - OK - initialized successfully"));

  Serial.begin(HW_SERIAL_BR);
  sw_serial_println(F("[HWS] - OK - initialized successfully"));
  
  for (Motor motor : MOTORS) {
    motor.init();
    motor.stop();
  }

  sw_serial_println(F("[MTR] - OK - initialized successfully"));

  for (Hall hall : halls) {
    hall.init();
  }

  PCICR |= B00000010;
  PCMSK1 |= B00001111;

  sw_serial_println(F("[HAL] - OK - initialized successfully"));
  
  Wire.begin();
  sw_serial_println(F("[TWI] - OK - initialized successfully"));

  srsc.registerOnPacketArrivedCallback(onPacketArrivedCallback);
  sw_serial_println(F("[SSC] - OK - on packet arrived callback registered successfully"));
  
  while (srsc.begin() != ReturnCode::OK) {
    delay(CONNECT_DELAY);
  }

  sw_serial_println(F("[SSC] - OK - connected successfully"));

  srsc.registerPacketType(REGISTER_SENSOR_PT, REGISTER_SENSOR_PS, Severity::CRITICAL);
  sw_serial_println(F("[SSC] - OK - 'sensor registration' packet type registered successfully"));
  srsc.registerPacketType(F_BUMPER_PT, BUMPER_PS, Severity::REPEATED);
  srsc.registerPacketType(B_BUMPER_PT, BUMPER_PS, Severity::REPEATED);
  sw_serial_println(F("[SSC] - OK - 'bumper contact' packet types registered successfully"));
  srsc.registerPacketType(FL_MOTOR_SPEED_PT, MOTOR_PS, Severity::NORMAL);
  srsc.registerPacketType(FR_MOTOR_SPEED_PT, MOTOR_PS, Severity::NORMAL);
  srsc.registerPacketType(BL_MOTOR_SPEED_PT, MOTOR_PS, Severity::NORMAL);
  srsc.registerPacketType(BR_MOTOR_SPEED_PT, MOTOR_PS, Severity::NORMAL);
  sw_serial_println(F("[SSC] - OK - 'motor speed' packet types registered successfully"));
  srsc.registerPacketType(FL_MOTOR_DIRECTION_PT, MOTOR_PS, Severity::NORMAL);
  srsc.registerPacketType(FR_MOTOR_DIRECTION_PT, MOTOR_PS, Severity::NORMAL);
  srsc.registerPacketType(BL_MOTOR_DIRECTION_PT, MOTOR_PS, Severity::NORMAL);
  srsc.registerPacketType(BR_MOTOR_DIRECTION_PT, MOTOR_PS, Severity::NORMAL);
  sw_serial_println(F("[SSC] - OK - 'motor direction' packet types registered successfully"));
  srsc.registerPacketType(HALL_PT, HALL_PS, Severity::NORMAL);
  sw_serial_println(F("[SSC] - OK - 'hall' packet type registered successfully"));

  attachInterrupt(digitalPinToInterrupt(FRONT_INTERRUPT_PIN), frontInterrupt, RISING);
  sw_serial_println(F("[INT] - OK - front interrupt registered successfully"));
  attachInterrupt(digitalPinToInterrupt(BACK_INTERRUPT_PIN), backInterrupt, RISING);
  sw_serial_println(F("[INT] - OK - back interrupt registered successfully"));

  protorduino.registerTask(bumperTask, BUMPER_TASK_DURATION);
  sw_serial_println(F("[PRO] - OK - bumper task registered successfully"));
  protorduino.registerTask(hallTask, HALL_TASK_DURATION);
  sw_serial_println(F("[PRO] - OK - hall task registered successfully"));
}

void loop() {
  protorduino.loop();
  srsc.loop();
}
