#ifndef MOTOR_H
#define MOTOR_H

#include <Arduino.h>

struct Motor {
  const byte PWM_PIN;
  const byte DIRECTION_PIN;

  void init() const;
  void forward() const;
  void backward() const;
  void stop() const;
  void setSpeed(byte speed) const;
};

#endif
