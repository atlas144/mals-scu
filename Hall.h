#ifndef HALL_H
#define HALL_H

#include <Arduino.h>

struct Hall {
  const byte PIN;
  byte counter = 0;

  Hall(byte pin);
  void init() const;
};

#endif