#ifndef TWI_DEVICE_H
#define TWI_DEVICE_H

#include <Arduino.h>

struct TwiDevice {
  byte twiAddress;
  byte* data;
  byte dataSize;
  byte srscPacketType;
};

TwiDevice* getDevice(byte srscPacketType);
void addDevice(byte srscPacketType, byte twiAddress, byte dataSize);

#endif
