#include "Motor.h"

void Motor::init() const {
  pinMode(PWM_PIN, OUTPUT);
  pinMode(DIRECTION_PIN, OUTPUT);
}

void Motor::forward() const {
  digitalWrite(DIRECTION_PIN, HIGH);
}

void Motor::backward() const {
  digitalWrite(DIRECTION_PIN, LOW);
}

void Motor::stop() const {
  analogWrite(PWM_PIN, 0);
}

void Motor::setSpeed(byte speed) const {
  analogWrite(PWM_PIN, speed);
}