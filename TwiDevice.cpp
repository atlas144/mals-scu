#include "TwiDevice.h"

byte twiDevicesLength = 0;
TwiDevice** twiDevices = new TwiDevice*[twiDevicesLength];

TwiDevice* getDevice(byte srscPacketType) {
  for (uint8_t i = 0; i < twiDevicesLength; ++i) {
    TwiDevice* device = twiDevices[i];

    if (device->srscPacketType == srscPacketType) {
      return device;
    }
  }

  return NULL;
}

void addDevice(byte srscPacketType, byte twiAddress, byte dataSize) {
  TwiDevice** newDevices = new TwiDevice*[++twiDevicesLength];

  for (byte i = 0; i < twiDevicesLength - 1; ++i) {
    newDevices[i] = twiDevices[i];
  }

  delete [] twiDevices;
  twiDevices = newDevices;

  TwiDevice* newDevice = new TwiDevice();

  newDevice->twiAddress = twiAddress;
  newDevice->dataSize = dataSize;
  newDevice->data = new byte[dataSize];
  newDevice->srscPacketType = srscPacketType;

  twiDevices[twiDevicesLength - 1] = newDevice;
}