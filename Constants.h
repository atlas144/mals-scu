#include <Arduino.h>
#include "Motor.h"
#include "Hall.h"

const byte MOTORS_NO = 4;
const byte HALLS_NO = 4;

const int BYTE_MAX = 256;

const long HW_SERIAL_BR = 115200;
const long SW_SERIAL_BR = 9600;

const int CONNECT_DELAY = 1000;
const int PROCESS_BUMPER_COLLISION_DELAY = 5;
const int PROCESS_HALL_MEASUREMENT_DELAY = 100;

// PINS

const byte FRONT_INTERRUPT_PIN = 2;
const byte BACK_INTERRUPT_PIN = 3;
const byte SS_RX_PIN = 4;
const byte SS_TX_PIN = 13;
const Motor MOTORS[] = {
  {5, 7},
  {6, 8},
  {9, 10},
  {11, 12}
};
Hall halls[] = {
  {14},
  {15},
  {16},
  {17}
};

// MOTORS

const byte MOTOR_FORWARD = 0;
const byte MOTOR_BACKWARD = 1;

// SENSORS

const byte FRONT_COLLISION = 0;
const byte BACK_COLLISION = 1;

const int BUMPER_TASK_DURATION = 1;
const int HALL_TASK_DURATION = 500;

// PAYLOAD SIZE

const byte REGISTER_SENSOR_PS = 5;
const byte BUMPER_PS = 0;
const byte MOTOR_PS = 1;
const byte HALL_PS = HALLS_NO;

// PACKET DATA

const byte MOTOR_SPEED_POSITION = 0;
const byte MOTOR_DIRECTION_POSITION = 0;

const byte REG_SRSC_PACKET_TYPE_POSITION = 0;
const byte REG_TWI_ADDRESS_POSITION = 1;
const byte REG_DATA_SIZE_POSITION = 2;
const byte REG_DURATION_LSB_POSITION = 3;
const byte REG_DURATION_MSB_POSITION = 4;

// PACKET TYPES

const byte REGISTER_SENSOR_PT = 0x10;

const byte FL_MOTOR_SPEED_PT = 0x20;
const byte FR_MOTOR_SPEED_PT = 0x21;
const byte BL_MOTOR_SPEED_PT = 0x22;
const byte BR_MOTOR_SPEED_PT = 0x23;
const byte FL_MOTOR_DIRECTION_PT = 0x24;
const byte FR_MOTOR_DIRECTION_PT = 0x25;
const byte BL_MOTOR_DIRECTION_PT = 0x26;
const byte BR_MOTOR_DIRECTION_PT = 0x27;

const byte F_BUMPER_PT = 0x30;
const byte B_BUMPER_PT = 0x31;
const byte HALL_PT = 0x32;